# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=W0222,W0223

"""eBPF Component Manager provides Business Logic for communication."""

import logging

from ska_tango_base.base import BaseComponentManager
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState


class EbpfComponentManager(BaseComponentManager):
    """eBPF Component Manager."""

    def __init__(
        self,
        logger: logging.Logger,
        communication_state_callback,  #: Callable[[CommunicationStatus]],
        component_state_callback,  #: Callable,
    ):
        """Initialise the component manager."""
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            fault=None,
        )

    def start_communicating(self):
        """Initialise the attributes and properties of the Performance Evaluator."""
        # Intended to start comms to a hardware device
        # Called when adminMode is changed to ONLINE
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._update_component_state(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        """Stop communications with the hardware."""
        # Intended to stop comms to a hardware device
        # Called when adminMode changed to OFFLINE
        self._update_component_state(power=PowerState.OFF, fault=False)
        self._update_communication_state(CommunicationStatus.DISABLED)

    def off(self, _):  # 2nd arg is "task_callback: Callable"
        """Turn off the component."""
        message = "Ignored OFF"
        self.logger.error(message)
        return ResultCode.REJECTED, message

    def standby(self, _):  # 2nd arg is "task_callback: Callable"
        """Standby the component."""
        message = "Ignored STANDBY"
        self.logger.error(message)
        return ResultCode.REJECTED, message

    def on(self, _):  # 2nd arg is "task_callback: Callable"
        """Turn on the component."""
        message = "Ignored ON"
        self.logger.error(message)
        return ResultCode.REJECTED, message

    def reset(self, _):  # 2nd arg is "task_callback: Callable"
        """Reset the component."""
        message = "Ignored RESET"
        self.logger.error(message)
        return ResultCode.REJECTED, message
