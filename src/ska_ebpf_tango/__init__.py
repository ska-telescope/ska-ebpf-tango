"""Top-level package for ska-ebpf-tango."""

__author__ = """Guillaume Jourjon"""
__email__ = "guillaume.jourjon@data61.csiro.au"
__version__ = "0.0.1"
