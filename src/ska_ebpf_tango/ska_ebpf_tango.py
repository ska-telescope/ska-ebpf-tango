# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=attribute-defined-outside-init

"""NetworkMonitor Tango Device provides a net monitoring service."""

__copyright__ = "Copyright (c) 2023, CSIRO"
__author__ = "Guillaume Jourjon"
__email__ = "guillaume.jourjon@data61.csiro.au"
__version__ = "0.0.1"

import json
import logging
from threading import Thread
from time import sleep

from bcc import BPF
from netaddr import IPAddress

# from prometheus_client import Counter, Gauge, Summary, start_http_server
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import ResultCode  # FastCommand
from tango import AttrWriteType
from tango.server import attribute, command, run

from ska_ebpf_tango.ebpf_component_manager import EbpfComponentManager

# dictionary of types for attributes
ATTR_TYPES = {
    "int": int,
    "float": float,
    "str": str,
    "bool": bool,
}


class NetworkMonitor(SKABaseDevice):
    """NetworkMonitor Tango Device provides a net monitoring service."""

    # -----------------
    # Device Properties
    # -----------------

    # -----------------
    # Attributes
    # -----------------

    @attribute(dtype=str)
    def interface_to_monitor(self):
        """
        Get Report rate.

        :return: float number
        """
        return self._interface_to_monitor

    # ---------------
    # General methods
    # ---------------

    def create_component_manager(self):
        """Create the component manager."""
        self.logger.info("run create_component_manager")
        return EbpfComponentManager(
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
        )

    def always_executed_hook(self):
        """Execute hook before any TANGO command is executed."""
        # PROTECTED REGION ID(LowCbfConnector.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  LowCbfConnector.always_executed_hook

    def delete_device(self):
        """Delete device allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(LowCbfConnector.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  LowCbfConnector.delete_device

    def _get_report(self, attr):
        """Read a dynamic report attribute."""
        name = attr.get_name()
        if name in self._list_attributes:
            return self._list_attributes[name]
        return None

    # -----------------
    # Commands
    # -----------------

    def __init__(self, *args, **kwargs):
        """Initialise the command handlers for commands supported by this device."""
        super().__init__(*args, **kwargs)

        self._size_of_report = 100
        self._report_rate = 10

        self.logger.warning("Init complete.")

    def init_command_objects(self):
        """Initialise the command handlers for commands supported by this device."""
        super().init_command_objects()
        self.logger.info("init_command_objects completed!")

    class InitCommand(SKABaseDevice.InitCommand):
        """Extend the SKABaseDevice class used for device initialisation."""

        def do(self, *args, **kwargs) -> tuple[ResultCode, str]:
            """Initialise the attributes and properties this device subclass."""
            # pylint: disable=protected-access
            super().do(*args, **kwargs)
            self._device.logger = logging.getLogger()
            self._device._interface_to_monitor = ""
            monitor_totals = attribute(
                name="monitor_totals",
                dtype=str,
                fget=self._device._get_report,
                fset=self._device._set_report,
                access=AttrWriteType.READ,
            )
            self._device.add_attribute(monitor_totals)
            self._device.set_change_event("monitor_totals", True, False)
            self._device.set_archive_event("monitor_totals", True, False)
            monitor_diff = attribute(
                name="monitor_diff",
                dtype=str,
                fget=self._device._get_report,
                fset=self._device._set_report,
                access=AttrWriteType.READ,
            )
            self._device.add_attribute(monitor_diff)
            self._device.set_change_event("monitor_diff", True, False)
            self._device.set_archive_event("monitor_diff", True, False)

            message = "Initialisation complete"
            self._device.logger.info(message)
            return ResultCode.OK, message

    @command()
    def start_monirotoring_thread(self):
        """Start the reporting thread."""
        if self._interface_to_monitor == "":
            message = "No interface specified"
            self.logger.error(message)
            return ResultCode.REJECTED, message
        self.byte_code_ebpf = BPF(src_file="monitor.c", debug=0)
        self.packet_monitor_fn = self.byte_code_ebpf.load_func(
            "packet_monitor", BPF.SOCKET_FILTER
        )
        BPF.attach_raw_socket(self.packet_monitor_fn, "wlo1")
        self.stats = self.byte_code_ebpf.get_table("stats")
        self.measurement_thread = Thread(target=self.monitor)
        self.measurement_thread_daemon = True
        self.measurement_thread.start()
        message = "Started reporting thread"
        self.logger.info(message)
        return ResultCode.OK, message

    def monitor(self):
        """Monitor the packets."""
        prev = {}

        while True:
            result_total = []
            result_delta = []
            tmp = {}
            # compute both the total and last-N-seconds statistics
            for k, v in self.stats.items():
                # subtract the previous totals from the current, or 0 if none exists
                v2 = self.delta_stats(
                    v, prev.get(self.key2str(k), self.stats.Leaf(0, 0))
                )
                if v2.pkts != 0 or v2.pkts != 0:
                    result_delta.append(self.stats2json(k, v2))
                tmp[self.key2str(k)] = v
                result_total.append(self.stats2json(k, v))
            prev = tmp
            print(result_total)
            print(result_delta)
            self.push_change_event("monitor_totals", json.dumps(result_total))
            self.push_archive_event("monitor_totals", json.dumps(result_total))
            self.push_change_event("monitor_diff", json.dumps(result_delta))
            self.push_archive_event("monitor_diff", json.dumps(result_delta))

            sleep(5)

    def stats2json(self, k, v):
        """Transform the stats table into a JSON object."""
        return {
            "proto": int(k.proto),
            "source_ip": str(IPAddress(k.sip)),
            "dest_ip": str(IPAddress(k.dip)),
            "source_port": str(k.sport),
            "dest_port": str(k.dport),
            "pkts": v.pkts,
            "bytes": v.pkts,
        }

    def delta_stats(self, v, oldv):
        """Compute the delta between two stats objects."""
        return self.stats.Leaf(
            v.pkts - oldv.pkts,
            v.bytes - oldv.bytes,
        )

    def key2str(self, k):
        """Format the key into a string."""
        return f"{IPAddress(k.sip)},{IPAddress(k.dip)},{k.proto},{k.sport},{k.dport}"


def main(args=None, **kwargs):
    """Start main function of the PerfEval module."""
    return run((NetworkMonitor,), args=args, **kwargs)


if __name__ == "__main__":
    main()
