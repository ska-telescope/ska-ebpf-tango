# ska-ebpf-tango

Tango Device to investigate eBPF within Tango


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ebpf-tango/badge/?version=latest)](https://developer.skao.int/projects/ska-ebpf-tango/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-ebpf-tango documentation](https://developer.skatelescope.org/projects/ska-ebpf-tango/en/latest/index.html "SKA Developer Portal: ska-ebpf-tango documentation")

## Features

* TODO
